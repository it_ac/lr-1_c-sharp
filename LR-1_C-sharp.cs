﻿using System;

namespace LR_1_v2
{
    class Program
    {
        static void Main(string[] args)
        {
            Print("Введите целое число: ");
            int Number = InputNumber();
            int Num1 = Num1Random(Number);
            int Num2 = Num2Random(Num1, Number);
            Console.Clear();
            Print("Введеное число: " + Number);
            Print("1. Количество цифр в веденном числе: " + NumberOfDigits(Number));
            Print("2. Массив: [" + string.Join(", ", Massif(Number)) + "]");
            Print("3. Среднее арифметическое: " + ArifmeticalMean(Number));
            Print("4. Среднее геометрическое: " + GeometricMean(Number));
            Print("5. Факториал: " + Factorial(Number));
            Print($"6. Сумма всех парных чисел от 1 до {Number}: {SumEven(Number)}");
            Print($"7. Сумма всех непарных чисел от 1 до {Number}: {SumOdd(Number)}");
            Print($"8.1 Сумма всех парных чисел от {Num1} до {Num2}: {SumEven(Num1, Num2)}");
            Print($"8.2 Сумма всех непарных чисел от {Num1} до {Num2}: {SumOdd(Num1, Num2)}\n");
            Print($"6.2 Сумма всех парных чисел от 1 до {Number}: {SumEven2(Number)}");
            Print($"6.3 Сумма всех парных чисел от 1 до {Number}: {SumEven3(Number)}");

            Console.ReadKey();
        }

        /// <summary>
        /// Функция для вывода в консоль
        /// </summary>
        /// <param name="str"></param>
        static void Print(string str)
        {
            Console.WriteLine($"{str}");
        }

        /// <summary>
        /// Функция для ввода целого числа с проверкой целое ли число
        /// </summary>
        /// <returns></returns>
        static int InputNumber()
        {
            int Number;
            while (!int.TryParse(Console.ReadLine(), out Number ))
            {
                Console.WriteLine("Ошибка ввода! Введите целое число:");
            }
            Number = Math.Abs(Number);
            return Number;
        }

        /// <summary>
        /// Функция для нахождения количества цифр в числе N.
        /// </summary>
        /// <param name="Number"></param>
        /// <returns></returns>
        static int NumberOfDigits(int Number)
        {
            int Ndigits = (int)Math.Ceiling(Math.Log10(Number));
            return Ndigits;
        }

        /// <summary>
        /// Функция организации массива, элементами которого есть цифры числа N.
        /// </summary>
        /// <param name="Number"></param>
        /// <returns></returns>
        static int[] Massif(int Number)
        {
            int[] mas = new int[NumberOfDigits(Number)];
            int j = NumberOfDigits(Number) - 1;
            int TNumber = Number;
            while (TNumber > 0)
            {
                mas[j--] = TNumber % 10;
                TNumber = TNumber / 10;
            }
            return mas;
        }

        /// <summary>
        /// Функция нахождения среднее арифметическое цифр числа N.
        /// </summary>
        /// <param name="Number"></param>
        /// <returns></returns>
        static double ArifmeticalMean(int Number)
        {
            double sum = 0;
            int[] mas = Massif(Number);
            double Ndigits = NumberOfDigits(Number);
            for (int i = 0; i < Ndigits; i++)
            {
                sum += mas[i];
            }
            double AMean = sum / Ndigits;
            return AMean;
        }

        /// <summary>
        /// Функция нахождения среднее геометрическое цифр числа N.
        /// </summary>
        /// <param name="Number"></param>
        /// <returns></returns>
        static double GeometricMean(int Number)
        {
            double dob = 1;
            int[] mas = Massif(Number);
            double Ndigits = NumberOfDigits(Number);
            for (int i = 0; i < Ndigits; i++)
            {
                dob *= mas[i];
            }
            double GMean = Math.Pow(dob,1/Ndigits);
            return GMean;
        }

        /// <summary>
        /// Функция нахождения факториала числа N.
        /// </summary>
        /// <param name="Number"></param>
        /// <returns></returns>
        static double Factorial(int Number)
        {
            double NFact = 1;
            for(int i = 1; i <= Number; i++)
            {
                NFact *= i;
            }
            return NFact;
        }

        /// <summary>
        /// Функция нахождения суму всех парных чисел от 1 до N.
        /// </summary>
        /// <param name="Number"></param>
        /// <returns></returns>
        static long SumEven(int Number)
        {
            long NSumEven = 0;
            for (int i = 0; i <= Number; i++)
                if (i % 2 == 0)
                {
                    NSumEven += i;
                }
            return NSumEven;
        }

        static long SumEven2(int Number)
        {
            long NSumEven = 0;
            int i = 0;
            do
            {
                if (i % 2 == 0)
                {
                    NSumEven += i;
                }
                i++;
            } while (i <= Number);
            return NSumEven;
        }

        static long SumEven3(int Number)
        {
            long NSumEven = 0;
            int i = 0;
            while(i<=Number)
            {
                if (i % 2 == 0)
                {
                    NSumEven += i;
                }
                i++;
            }
            return NSumEven;
        }

        /// <summary>
        ///  Функция нахождения суму всех непарных чисел от 1 до N.
        /// </summary>
        /// <param name="Number"></param>
        /// <returns></returns>
        static long SumOdd(int Number)
        {
            long NSumOdd = 0;
            for (int i = 1; i <= Number; i += 2)
                if (i % 2 != 0)
                {
                    NSumOdd += i;
                }
            return NSumOdd;
        }

        /// <summary>
        /// Функция нахождения суму всех парных чисел от Num1 до Num2(перегрузка функции SumEven).
        /// </summary>
        /// <param name="Num1"></param>
        /// <param name="Num2"></param>
        /// <returns></returns>
        static long SumEven(int Num1, int Num2)
        {
            long NsumEven = 0;
            for (int i = Num1; i <= Num2; i++)
                if (i % 2 == 0)
                {
                    NsumEven += i;
                }
            return NsumEven;
        }

        /// <summary>
        /// Функция нахождения суму всех непарных чисел от Num1 до Num2(перегрузка функции SumOdd).
        /// </summary>
        /// <param name="Num1"></param>
        /// <param name="Num2"></param>
        /// <returns></returns>
        static long SumOdd(int Num1, int Num2)
        {
            long NsumOdd = 0;
            for (int i = Num1; i <= Num2; i++)
                if (i % 2 != 0)
                {
                    NsumOdd += i;
                }
            return NsumOdd;
        }


        static int Num1Random(int Number)
        {
            Random rund = new Random();
            int Num1 = rund.Next(1, Number);
            int Num2 = rund.Next(Num1, Number);
            return Num1;
        }
        static int Num2Random(int Num1, int Number)
        {
            Random rund = new Random();
            int Num2 = rund.Next(Num1, Number);
            return Num2;
        }

    }
}
